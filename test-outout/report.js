$(document).ready(function() {var formatter = new CucumberHTML.DOMFormatter($('.cucumber-report'));formatter.uri("file:Features/verifyInformation.feature");
formatter.feature({
  "name": "Verification of Personal details for correct credentials",
  "description": "",
  "keyword": "Feature"
});
formatter.scenario({
  "name": "Verify successfull navigation to Idenetiy page after login",
  "description": "",
  "keyword": "Scenario",
  "tags": [
    {
      "name": "@tagDemo"
    }
  ]
});
formatter.before({
  "status": "passed"
});
formatter.step({
  "name": "User is on login page",
  "keyword": "Given "
});
formatter.match({
  "location": "stepDefination.LoginStepLibrary.launchLoginPage()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User see title of web page is \"My Store\"",
  "keyword": "When "
});
formatter.match({
  "location": "stepDefination.LoginStepLibrary.verifyTitle(java.lang.String)"
});
formatter.result({
  "error_message": "org.junit.ComparisonFailure: expected:\u003c[My Store]\u003e but was:\u003c[automationpractice.com]\u003e\r\n\tat org.junit.Assert.assertEquals(Assert.java:115)\r\n\tat org.junit.Assert.assertEquals(Assert.java:144)\r\n\tat stepDefination.LoginStepLibrary.verifyTitle(LoginStepLibrary.java:32)\r\n\tat ✽.User see title of web page is \"My Store\"(file:///C:/Users/Devesh/git/bdd/bdd/Features/verifyInformation.feature:6)\r\n",
  "status": "failed"
});
formatter.step({
  "name": "User clicks on sign in link",
  "keyword": "And "
});
formatter.match({
  "location": "stepDefination.LoginStepLibrary.clickOnSignInLink()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "User enters email and password",
  "rows": [
    {}
  ],
  "keyword": "And "
});
formatter.match({
  "location": "stepDefination.LoginStepLibrary.enterCredentials(io.cucumber.datatable.DataTable)"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "User clicks on sign in button",
  "keyword": "Then "
});
formatter.match({
  "location": "stepDefination.LoginStepLibrary.clickOnSignInButton()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "User see title of web page is \"My account - My Store\"",
  "keyword": "When "
});
formatter.match({
  "location": "stepDefination.LoginStepLibrary.verifyTitle(java.lang.String)"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "User clicks on My personal information tab",
  "keyword": "And "
});
formatter.match({
  "location": "stepDefination.LoginStepLibrary.clickOnTab()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "User navigates to Identity page",
  "keyword": "Then "
});
formatter.match({
  "location": "stepDefination.LoginStepLibrary.navigateToIdentityPage()"
});
formatter.result({
  "status": "skipped"
});
formatter.after({
  "status": "passed"
});
});
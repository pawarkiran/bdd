$(document).ready(function() {var formatter = new CucumberHTML.DOMFormatter($('.cucumber-report'));formatter.uri("C:/Users/Devesh/eclipse-workspace/TestAutomation/Features/login.feature");
formatter.feature({
  "line": 1,
  "name": "Verification of error for incorrect credentials",
  "description": "",
  "id": "verification-of-error-for-incorrect-credentials",
  "keyword": "Feature"
});
formatter.scenario({
  "line": 3,
  "name": "Verfication of error for incorrect credentials on My Store application",
  "description": "",
  "id": "verification-of-error-for-incorrect-credentials;verfication-of-error-for-incorrect-credentials-on-my-store-application",
  "type": "scenario",
  "keyword": "Scenario"
});
formatter.step({
  "line": 4,
  "name": "User is on login page",
  "keyword": "Given "
});
formatter.step({
  "line": 5,
  "name": "User see title of web page is \"My Store\"",
  "keyword": "When "
});
formatter.step({
  "line": 6,
  "name": "User clicks on sign in link",
  "keyword": "And "
});
formatter.step({
  "line": 7,
  "name": "User enters email and password",
  "rows": [
    {
      "cells": [
        "email",
        "password"
      ],
      "line": 8
    },
    {
      "cells": [
        "demo@eml.com",
        "pwd@123"
      ],
      "line": 9
    }
  ],
  "keyword": "And "
});
formatter.step({
  "line": 10,
  "name": "User clicks on sign in button",
  "keyword": "And "
});
formatter.step({
  "line": 11,
  "name": "User verifies error as \"There is 1 error\"",
  "keyword": "Then "
});
formatter.match({
  "location": "Login.user_is_on_login_page()"
});
formatter.result({
  "duration": 19156534100,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "My Store",
      "offset": 31
    }
  ],
  "location": "Login.user_verifies_title_of_web_page(String)"
});
formatter.result({
  "duration": 32330200,
  "error_message": "cucumber.api.PendingException: TODO: implement me\r\n\tat stepDefination.Login.user_verifies_title_of_web_page(Login.java:35)\r\n\tat ✽.When User see title of web page is \"My Store\"(C:/Users/Devesh/eclipse-workspace/TestAutomation/Features/login.feature:5)\r\n",
  "status": "pending"
});
formatter.match({
  "location": "Login.user_clicks_on_sign_in_link()"
});
formatter.result({
  "status": "skipped"
});
formatter.match({
  "location": "Login.user_enters_email_and_password(DataTable)"
});
formatter.result({
  "status": "skipped"
});
formatter.match({
  "location": "Login.user_clicks_on_sign_in_button()"
});
formatter.result({
  "status": "skipped"
});
formatter.match({
  "arguments": [
    {
      "val": "There is 1 error",
      "offset": 24
    }
  ],
  "location": "Login.user_verifies_error(String)"
});
formatter.result({
  "status": "skipped"
});
});
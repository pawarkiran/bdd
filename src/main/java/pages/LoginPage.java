package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class LoginPage {

	WebDriver driver;

	public LoginPage(WebDriver driver) {
		this.driver = driver;
		PageFactory.initElements(driver, this);
	}

	public void initailze() {
		driver.manage().window().maximize();
		driver.get("http://automationpractice.com/index.php");

	}

	@FindBy(xpath = "//a[contains(text(),'Sign in')]")
	WebElement buttonSignIn;

	@FindBy(xpath = "//span[contains(text(),'My personal information')]//parent::a")
	WebElement linkMyPersonalInformation;

	@FindBy(xpath = "//*[contains(text(),'Already registered?')]//parent::form//child::input[@name='email']")
	WebElement textBoxEmailAddress;

	@FindBy(xpath = "//label[contains(text(),'Password')]//parent::div//child::input")
	WebElement textBoxPassword;

	@FindBy(xpath = "//button[@id='SubmitLogin']")
	WebElement buttonLogin;

	@FindBy(xpath = "//img[@alt='My Store']")
	WebElement imgLogo;

	@FindBy(xpath = "//p[contains(text(),'There is 1 error')]")
	WebElement textError;

	public void clickSignON() {
		buttonSignIn.click();
	}

	public void clickLogin() {
		buttonLogin.click();
	}

	public void data(String email, String pswd) {
		textBoxEmailAddress.sendKeys(email);
		textBoxPassword.sendKeys(pswd);
	}

	public void clickOnTab()

	{
		linkMyPersonalInformation.click();
	}

	public String getTextError() {
		return textError.getText();
	}

	public boolean getImgLogo() {
		return imgLogo.isDisplayed();
	}
}

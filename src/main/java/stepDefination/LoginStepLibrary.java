
package stepDefination;

import java.util.List;
import java.util.concurrent.TimeUnit;

import org.junit.Assert;
import org.openqa.selenium.WebDriver;

import io.cucumber.datatable.DataTable;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import pages.LoginPage;

public class LoginStepLibrary {

	WebDriver driver = Hooks.driver;
	LoginPage login;

	@Given("User is on login page")
	public void launchLoginPage() {
		login = new LoginPage(driver);
		login.initailze();

	}

	@Then("User see title of web page is {string}")
	public void verifyTitle(String title) {
		Assert.assertEquals(title, driver.getTitle());
	}

	@And("User clicks on sign in link")
	public void clickOnSignInLink() {
		login.clickSignON();
	}

	@And("User enters email and password")
	public void enterCredentials(DataTable credentials) {
		System.out.println("Entered in data fun");
		List<List<String>> data = credentials.cells();
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		login.data(data.get(0).get(0), data.get(0).get(1));
	}

	@When("User enters {string} and {string}")
	public void enterCrecentialsParamters(String email, String password) {
		login.data(email, password);
	}

	@And("User clicks on login in button")
	public void clickOnSignInButton() {
		login.clickLogin();
	}

	@And("User clicks on My personal information tab")
	public void clickOnTab() {
		login.clickOnTab();
	}

	@Then("User navigates to Identity page")
	public void navigateToIdentityPage() {
		Assert.assertEquals("Identity - My Store", driver.getTitle());
	}

	@Then("User verifies error")
	public void verifyError() {
		Assert.assertEquals("There is 1 error", login.getTextError());

	}

	@Then("user is on home page")
	public void verifyUserIsOnHomePage() {
		login.getImgLogo();
	}

}

package stepDefination;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import io.cucumber.java.After;
import io.cucumber.java.Before;

public class Hooks {

	static WebDriver driver;

	@Before
	public void intializeTheBrowser() {
		System.setProperty("webdriver.chrome.driver", "resource\\chromedriver.exe");
		driver = new ChromeDriver();

	}

	@After
	public void QuitDriver() {
		driver.quit();
	}

}

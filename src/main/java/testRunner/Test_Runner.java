package testRunner;

import org.junit.runner.RunWith;

import io.cucumber.junit.Cucumber;
import io.cucumber.junit.CucumberOptions;

@RunWith(Cucumber.class)
@CucumberOptions(features = "Features", glue = { "stepDefination" }, plugin = { "pretty", "html:test-outout",
		"json:json_output/cucumber.json",
		"junit:junit_xml/cucumber.xml" }, monochrome = true, dryRun = false, tags = { "@Outline" })

public class Test_Runner {
}

Feature: Verification of error for incorrect credentials

  @paratmeter
  Scenario: Login with incorrect credentails
    Given User is on login page
    When User see title of web page is "My Store"
    And User clicks on sign in link
    When User enters "wrong@eml.com" and "wrongPwd"
    And User clicks on login in button
    Then User verifies error


  @Outline
  Scenario Outline: Login with correct credentails
    Given User is on login page
    When User see title of web page is "My Store"
    And User clicks on sign in link
    When User enters "<email>" and "<password>"
   And User clicks on login in button
    Then user is on home page

    Examples: 
      | email         | password |
      | monu@demo.com | monu@123 |
 

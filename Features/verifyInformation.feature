Feature: Verification of Personal details for correct credentials

  @DataTable
  Scenario: Verify successfull navigation to Idenetiy page after login
    Given User is on login page
    When User see title of web page is "My Store"
    And User clicks on sign in link
    And User enters email and password
      | monu@demo.com | monu@123 |
    And User clicks on login in button
    Then User see title of web page is "My account - My Store"
    And User clicks on My personal information tab
    Then User navigates to Identity page
